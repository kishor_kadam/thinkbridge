const express = require('express')
const utils = require('../utils')
const db = require('../db')
const router = express.Router()


 router.get('/all',(request,response)=>{
    const statement = `select id,Name,Description,Price,isActive from Product;`
    db.query(statement,(error,data)=>{
        const products = []
      for (let index = 0; index < data.length; index++) {
        const temp = data[index];
        const product = {
          id:temp['id'],
          name: temp['Name'],
          description: temp['Description'],
          price: temp['Price'],
          isActive:temp['isActive']
        }
        products.push(product);
    }
    response.send(utils.createResult(error,products));
    })
 })

 router.get('/details/:id',(request,response)=>{
    const {id}=request.params
    const statement = `select id,Name,Description,Price,isActive from Product where id = '${id}';`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
 })

 router.post('/create', (request, response) => {
    const {name,description,price} = request.body
    const statement = `INSERT INTO Product (Name,Description,Price) VALUES ('${name}', '${description}', '${price}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

  router.put('/:id', (request, response) => {
    const {id}=request.params
    const {name,description,price} = request.body
    const statement = `update Product set Name = '${name}' ,Description = '${description}',Price='${price}' where id = '${id}'`
    db.query(statement,(error,data)=>{
    response.send(utils.createResult(error,data))
    })
  })

  router.put('/:id/:isactive', (request, response) => {
    const {id}=request.params
    const {isactive}=request.params
    const statement = `update Product set isActive='${isactive}' where id = '${id}'`
    db.query(statement,(error,data)=>{
    response.send(utils.createResult(error,data))
    })
  })

  router.delete('/:id', (request, response) => {
    const {id}=request.params
    const statement = `DELETE FROM Product where id = '${id}'`
    db.query(statement,(error,data)=>{
    response.send(utils.createResult(error,data))
    })
  })

module.exports = router