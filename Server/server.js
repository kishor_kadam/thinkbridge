
//include node modules
const express = require('express')
const ProductRouter = require('./routes/product')
const cors = require('cors')
const app = express()

app.use(cors())
app.use(express.json())
app.use('/product', ProductRouter)

//listen on port 8080
app.listen(8080, '0.0.0.0', () => {
    console.log("Server started on port 8080")
})