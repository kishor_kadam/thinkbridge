import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = 'http://localhost:8080/product'

  constructor(private httpClient:HttpClient) { 
  }

  getProducts() {
    return this.httpClient.get(this.url + '/all' );
  }

  toggleActivateStatus(product) {
    const body = {}
    return this.httpClient.put(this.url + `/${product['id']}/${product['isActive'] == 0 ? 1 : 0}`, body)
  }
  
  getProductDetails(id) {
   return this.httpClient.get(this.url + "/details/" + id)
  }
  
  
  updateProduct(id,Name: string, description: string, price: number) {
  
   const body = {
     name:Name,
     description: description,
     price: price
   }
   
   return this.httpClient.put(this.url + "/" + id, body)
  }
  
  addProduct(name: string, description: string, price: number){
    const body = {
      name: name,
      description: description,
      price: price
    }
    console.log(body)
    return this.httpClient.post(this.url + "/create",body)
  }

  deleteProduct(product) {
    return this.httpClient.delete(this.url + '/'+ product['id'])
  }
  
}
