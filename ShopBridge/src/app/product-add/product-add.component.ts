import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
  id =null;
  name = ''
  description = ''
  price = 0
  product = null
 
  constructor(
    private router: Router,private activatedRoute: ActivatedRoute,
    private productService: ProductService) { }


  
    ngOnInit(): void {
      const name = this.activatedRoute.snapshot.queryParams['id']

      if (name) {
        // edit product
        this.productService
          .getProductDetails(name)
          .subscribe(response => {
            if (response['status'] == 'success') {
              const products = response['data']
              console.log(products)
              if (products.length > 0) {
                this.product = products[0];
                this.id = this.product['id']
                this.name = this.product['Name']
                this.description = this.product['Description']
                this.price = this.product['Price']
              }
            }
          })
      }
    }

    
  onUpdate() {
    if(this.product){
      this.productService
      .updateProduct(this.id ,this.name, this.description, this.price)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/product-list'])
        }
      })
    }else{
      this.productService.addProduct(this.name, this.description, this.price)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.router.navigate(['/product-list'])
        }
      })
    }
  }
}
